{
  inputs,
  pkgs,
  ...
}: {
  imports = [inputs.my-nixpkgs.devenvModules.personal];

  languages = {
    nix.enable = true;
    texlive = {
      enable = true;
      packages = tl: {inherit (tl) scheme-full;};
      latexmk.enable = true;
    };
  };

  packages = with pkgs; [tikzit gnumake42];
  processes.latexmk.exec = "latexmk -pvc -pdflua";
  scripts.makefig.exec = "latexmk -pdf ../figure.tex";
}
