open Monoidal_transducer

module MakeTest (M : Monoid.S) = struct
  include M

  module MOpt = Monoid.MakeOpt (M)
  module T = Transducer.Make (M)

  let print = Format.printf "@.%a@." T.fprint
end


module TrMon = Monoid.TraceMonoid (struct
    let independant x y = x - y <= 1 && y - x <= 1
  end)
module TraceMonoidTest = MakeTest (TrMon)

let () = Format.printf "Testing minimization of transducers over a trace monoid ...@."
let () = TraceMonoidTest.(
    let q0 = (0, Some TrMon.(gen 1)) in
    let q_unreachable = (1, Some TrMon.one) in
    let q_no_output = (2, None) in
    let q0_bis = (3, Some TrMon.(gen 1)) in

    let st = T.(singleton q0 TrMon.one
             |> add_state q_unreachable
             |> add_state q_no_output
             |> add_transition q0 0 TrMon.(gen 0) q0_bis
             |> add_transition q0_bis 0 TrMon.(gen 0) q0_bis
             |> add_transition q0 1 TrMon.one q_no_output) in
    let st1 = T.reach st in
    let st2 = T.total st1 in
    let st3 = T.prefix st2 in
    let st4 = T.merge st3 in

    assert (not (T.G.mem_vertex st1.graph q_unreachable)) ;
    assert (not (T.G.mem_vertex st2.graph q_no_output)) ;
    assert (match st3.input with
        | Some ((0, t0), u0) -> TrMon.(u0 =~ gen 1) && MOpt.(t0 =~ one)
        | _ -> false) ;
    assert (T.G.nb_vertex st4.graph = 1) ;
  )

module ZplusTest = MakeTest (Monoid.Zplus)
    
let () = Format.printf "Testing minimisation of transducers over Z ...@."
let () = ZplusTest.(
    let q0 = (0, None) in
    let q1 = (1, Some Z.zero) in
    let q2 = (2, Some Z.one) in

    let st = T.(singleton q0 Z.zero
             |> add_state q1
             |> add_state q2
             |> add_transition q0 0 Z.zero q1
             |> add_transition q0 1 Z.zero q2
             |> add_transition q1 0 Z.one q1
             |> add_transition q2 0 Z.one q2) in
    let st_min = T.merge st in
    assert (T.G.nb_vertex st_min.graph = 2) ;
  )

let () = ZplusTest.(
    let q0 = (0, None) in
    let q1 = (1, None) in
    let q2 = (2, None) in
    let q3 = (3, Some Z.zero) in

    let st = T.(singleton q0 Z.zero
                 |> add_state q1
                 |> add_state q2
                 |> add_state q3
                 |> add_transition q0 0 Z.zero q1
                 |> add_transition q0 1 Z.zero q2
                 |> add_transition q1 0 Z.one q3
                 |> add_transition q2 0 Z.one q3) in
    let st_min = T.min st in
    assert (T.G.nb_vertex st_min.graph = 3) ;
  )
