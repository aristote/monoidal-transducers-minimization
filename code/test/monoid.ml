open Monoidal_transducer.Monoid

module MakeFree (M : Free) =
struct
  include M

  let a = gen 0
  let b = gen 1
  let c = gen 2

  let test name condition =
    Format.printf "Testing %s ... " name ;
    try
      assert (condition ());
      Format.printf "success.@."
    with
    | Assert_failure _ -> Format.printf "FAILURE.@."
    | _ -> Format.printf "ERROR.@."

  let runtest name =
    Format.printf "-- Testing properties of %s. --@." name ;
    (* monoid identities *)
    test "left unit" (fun () -> one * a =~ a) ;
    test "right unit" (fun () -> a * one =~ a) ;
    test "associativity" (fun () -> (a * b) * c =~ a * (b * c)) ;

    (* basic functions *)
    test "power function" (fun () -> a ** 2 =~ a * a) ;
    test "free structure" (fun () -> a <>~ b) ;
    test "equality" (fun () -> a =~ a) ;

    (* divisibility *)
    test "gcd idempotence" (fun () -> a ^ a =~ a) ;
    test "gcd" (fun () -> (a * b) ^ (a * c) =~ a) ;
    test "division" (fun () -> left_divide a (a * b) =~ b) ;
    test "division error" (fun () ->
        try
          let d = left_divide a b in
          (* this forces d to be fully evaluated *)
          ignore Format.(fprintf str_formatter "%a" fprint d) ;
          false
        with DivisionError -> true) ;
    test "equality implying left equivalence" (fun () ->
        left_equivalence a a =~ one) ;
    test "equality implying left equivalence" (fun () ->
        right_equivalence a a =~ one) ;

    Format.printf "-- Testing advanced properties : failure may be expected. --@." ;
    (* commutativity *)
    test "commutativity" (fun () -> (a * b =~ b * a)) ;
    test "commutative gcd" (fun () -> (a * c * b) ^ (b * a) =~ a * b) ;
    test "commutative division" (fun () ->
        try left_divide b (a * c * b) =~ a * c
        with DivisionError -> false) ;


    Format.printf "-- Finished testing %s. --@.@." name
end

module FreeMonoidTest = MakeFree (FreeMonoid)
let () = FreeMonoidTest.runtest "the free monoid"

module FreeCommutativeMonoidTest = MakeFree (FreeCommutativeMonoid)
let () = FreeCommutativeMonoidTest.runtest "the free commutative monoid"

module SomeTraceMonoid = TraceMonoid (struct let independant x y = x - y <= 1 && x - y >= -1 end)
module TraceMonoidTest = MakeFree (SomeTraceMonoid)
let () = TraceMonoidTest.runtest "some trace monoid"

