exception DivisionError
exception NotEquivalent

module type S =
sig
  type t

  val one : t
  val ( * ) : t -> t -> t
  val ( ** ) : t -> int -> t
  val ( =~ ) : t -> t -> bool
  val ( <>~ ) : t -> t -> bool

  val ( ^ ) : t -> t -> t
  val left_divide : t -> t -> t

  val is_left_equivalent : t -> t -> bool
  val left_equivalence : t -> t -> t
  val is_right_equivalent : t -> t -> bool
  val right_equivalence : t -> t -> t

  val fprint : Format.formatter -> t -> unit
end

module type Free =
sig
  include S

  val gen : int -> t
  val prepend : int -> t -> t
  val append : t -> int -> t
end


module FreeMonoid =
struct
  module M = BatLazyList
  type t = int M.t

  let one = M.nil 
  let ( * ) = M.append
  let ( ** ) w n = M.concat (M.make n w)
  let gen = M.make 1
  let prepend = M.cons
  let append w a = w * gen a 
  let ( =~ ) = M.equal ( = )
  let ( <>~ ) u v = not (u =~ v)

  let rec ( ^ ) w1 w2 = match w1, w2 with
    | lazy M.(Cons (a1, u1)), lazy M.(Cons (a2, u2)) when a1 = a2 ->
      M.cons a1 (u1 ^ u2)
    | _ -> one

  let rec left_divide w1 w2 = match w1, w2 with
    | lazy M.Nil, _ -> w2
    | lazy M.(Cons (a1, u1)), lazy M.(Cons (a2, u2)) when a1 = a2 ->
      left_divide u1 u2
    | _ -> raise DivisionError

  let left_equivalence w1 w2 = if w1 =~ w2 then one else raise NotEquivalent
  let is_left_equivalent = ( =~ )
  let right_equivalence = left_equivalence
  let is_right_equivalent = is_left_equivalent

  let fprint fmt m = if m =~ one then
      Format.fprintf fmt "ε"
    else
      M.iter (Format.fprintf fmt "@[(%d)@]") m
end

(* a module of maps to natural numbers *)
module NatMap (A : Map.OrderedType) =
struct
  module M = Map.Make(A)
  include M
  type t = int M.t

  let equal = equal ( = )

  let (let*) = Option.bind
  let (let+) x f = Option.map f x

  let mergeop (binop : int -> int -> int) = merge (fun _ x y ->
      let* m = x in
      let+ n = y in
      binop m n)

  let min = mergeop Stdlib.min
  let add a n = update a (function | Some m -> Some (m + n)
                                   | None -> Some n)
  let diff = merge (fun _ m n -> match m, n with
      | Some x, Some y when x < y -> Some (y - x)
      | Some x, Some y when x = y -> None
      | None, Some y -> Some y
      | None, None -> None
      | _ -> raise (Invalid_argument "cannot have negative natural numbers"))
end

module FreeCommutativeMonoid =
struct
  module M = NatMap(Int)
  type t = M.t

  let one = M.empty
  let ( * ) = M.merge (fun _ m n -> match m, n with
      | Some x, Some y -> Some (x + y)
      | Some x, _
      | _, Some x -> Some x
      | _ -> None)
  let ( ** ) w n = M.map Int.(mul n) w
  let gen n = M.singleton n 1
  let prepend a = M.add a 1
  let append w a = prepend a w
  let ( =~ ) = M.equal
  let ( <>~ ) u v = not (u =~ v)

  let ( ^ ) = M.min
  let left_divide d w = try M.diff d w with Invalid_argument _ -> raise DivisionError

  let left_equivalence w1 w2 = if w1 =~ w2 then one else raise NotEquivalent
  let is_left_equivalent = ( =~ )
  let right_equivalence = left_equivalence
  let is_right_equivalent = is_left_equivalent

  let fprint fmt m = if m =~ one then
      Format.fprintf fmt "ε"
    else
      M.iter (fun a n ->
          Format.fprintf fmt "@[" ;
          for _ = 1 to n do
            Format.fprintf fmt "@[(%d)@]" a
          done ;
          Format.fprintf fmt "@]")
        m
end

module TraceMonoid (E : sig val independant : int -> int -> bool end) =
struct
  module M = BatLazyList
  module NatMap = NatMap (Int)

  type t = int M.t

  let one = M.nil
  let ( * ) = M.append
  let ( ** ) w n = M.concat (M.make n w)
  let gen = M.make 1
  let prepend = M.cons
  let append w a = w * gen a

  let of_natmap m = NatMap.fold (fun a n w -> w * (gen a ** n)) m one

  let ( ^ ) =
    let rec traverse head1 head2 tail1 tail2 = 
      let rec first_letters head tail seen = function
        | lazy M.Nil -> head, tail
        | lazy M.(Cons (a, u)) ->
          let head, tail = if M.for_all (E.independant a) seen then
              NatMap.add a 1 head, tail
            else
              head, append tail a in
          first_letters head tail (append seen a) u in
      let head1, tail1 = first_letters head1 one one tail1 in
      let head2, tail2 = first_letters head2 one one tail2 in
      let cd = NatMap.min head1 head2 in
      if NatMap.is_empty cd then
        one
      else
        (of_natmap cd) * (traverse NatMap.(diff cd head1) NatMap.(diff cd head2) tail1 tail2)
    in traverse NatMap.empty NatMap.empty

  let rec left_divide_letter a = function
    | lazy M.(Cons (b, u)) when a = b ->
      u
    | lazy M.(Cons (b, u)) when E.independant a b ->
      prepend b (left_divide_letter a u)
    | _ -> raise DivisionError

  let rec left_divide d w = match d with
    | lazy M.Nil -> w
    | lazy M.(Cons (a, u)) -> left_divide u (left_divide_letter a w)

  let ( =~ ) w1 w2 =
    try
      match left_divide w1 w2 with
      | lazy M.Nil -> true
      | _ -> false
    with DivisionError -> false
  let ( <>~ ) u v = not (u =~ v)

  let left_equivalence w1 w2 = if w1 =~ w2 then one else raise NotEquivalent
  let is_left_equivalent = ( =~ )
  let right_equivalence = left_equivalence
  let is_right_equivalent = is_left_equivalent

  let fprint fmt m = if m =~ one then
      Format.fprintf fmt "ε"
    else
      M.iter (Format.fprintf fmt "@[(%d)@]") m
end


module Zplus = struct
  type t = Z.t

  let one = Z.zero
  let ( * ) = Z.(+)
  let ( ** ) n p = Z.(n * of_int p)
  let ( =~ ) = Z.equal
  let ( <>~ ) n1 n2 = not (n1 =~ n2)

  let ( ^ ) _ _ = one
  let left_divide d n = Z.(n - d)

  let is_left_equivalent _ _ = true
  let left_equivalence = Z.sub
  let is_right_equivalent _ _ = true
  let right_equivalence = Z.sub

  let fprint = Z.pp_print
end


module MakeOpt (M : S) =
struct
  let (let*) = Option.bind
  let (let+) x f = Option.map f x
  let binop_opt binop m1 m2 =
    let* m1 = m1 in
    let+ m2 = m2 in
    binop m1 m2

  type t = M.t option

  let one = Some M.one
  let ( * ) = binop_opt M.( * )
  let ( ** ) m n =
    let+ m = m in
    M.(m ** n)
  let make = Option.some
  let ( =~ ) = Option.equal M.( =~ )
  let ( <>~ ) = Option.equal M.( <>~ )

  let ( ^ ) m1 m2 = match m1, m2 with
    | Some m1, Some m2 -> Some M.(m1 ^ m2)
    | Some m, _
    | _, Some m -> Some m
    | _ -> None

  let left_divide m1 m2 = match m1, m2 with
    | Some m1, Some m2 -> Some M.(left_divide m1 m2)
    | _, None -> None
    | _ -> raise DivisionError

  let left_equivalence m1 m2 = match m1, m2 with
    | Some m1, Some m2 -> Some M.(left_equivalence m1 m2)
    | None, None -> one
    | _ -> raise NotEquivalent
  let is_left_equivalent m1 m2 = match m1, m2 with
    | Some m1, Some m2 -> M.(is_left_equivalent m1 m2)
    | None, None -> true
    | _ -> false

  let right_equivalence m1 m2 = match m1, m2 with
    | Some m1, Some m2 -> Some M.(right_equivalence m1 m2)
    | None, None -> one
    | _ -> raise NotEquivalent
  let is_right_equivalent m1 m2 = match m1, m2 with
    | Some m1, Some m2 -> M.(is_right_equivalent m1 m2)
    | None, None -> true
    | _ -> false

  let fprint fmt = function
    | Some m -> M.fprint fmt m
    | None -> Format.fprintf fmt "⊥"
end

module MakeProduct (M : S) (N : S) =
struct
  type t = M.t * N.t

  let make m n = m, n
  let map f g (m, n) = f m, g n
  let map2 f g (m1, n1) (m2, n2) = f m1 m2, g n1 n2
  let for_all2 f g (m1, n1) (m2, n2) = f m1 m2 && g n1 n2
  let exists2 f g (m1, n1) (m2, n2) = f m1 m2 || g n1 n2

  let one = M.one, N.one
  let ( * ) = map2 M.( * ) N.( * )
  let ( ** ) (m, n) p = M.(m ** p), N.(n ** p)
  let ( =~ ) = for_all2 M.( =~ ) N.( =~ )
  let ( <>~ ) = exists2 M.( <>~ ) N.( <>~ )

  let ( ^ ) = map2 M.( ^ ) N.( ^ )
  let left_divide = map2 M.left_divide N.left_divide

  let left_equivalence = map2 M.left_equivalence N.left_equivalence
  let is_left_equivalent = for_all2 M.is_left_equivalent N.is_left_equivalent
  let right_equivalence = map2 M.right_equivalence N.right_equivalence
  let is_right_equivalent = for_all2 M.is_right_equivalent N.is_right_equivalent

  let fprint fmt (m, n) = Format.fprintf fmt "@[(%a, %a)@]" M.fprint m N.fprint n
end
