exception DivisionError
exception NotEquivalent

(** The signature of an arbitrary monoid verifying
    - left-cancellativity up to invertibles on the left ;
    - right-cancellativity ; 
    - existence of a unique greatest common left-divisor up to invertibles on
      the left. *)
module type S =
sig
  (** {2 Monoidal structure} *)

  (** The type of elements of the monoid. *)
  type t

  (** The unit of the monoid. *)
  val one : t

  (** The associative operation of the monoid. *)
  val ( * ) : t -> t -> t

  (** Iterated application of the associative operation.
      [m ** (n+1)] is the same as [m * (m ** n)] and [m ** 0] is [one]. *)
  val ( ** ) : t -> int -> t

  (** Equality of elements inside the monoid. *)
  val ( =~ ) : t -> t -> bool
  val ( <>~ ) : t -> t -> bool

  (** {2 Division} *)

  (** [m ^ n] is the greatest common left-divisors of [m] and [n]
      (unique up to invertibles on the left). *)
  val ( ^ ) : t -> t -> t

  (** [left_divide d m] returns the (unique up to invertibles on the left) [n]
      such that [m = d * n] or raises [DivisionError] if such an [n] does not
      exist. *)
  val left_divide : t -> t -> t

  (** {2 Equivalence up to invertibles} *)

  (** [is_left_equivalent m n] returns [true] if and only if there is an
      invertible [x] such that [m = x * n] *)
  val is_left_equivalent : t -> t -> bool

  (** [left_equivalence m n] returns an invertible [x] such that [m = x * n] or
      raises [NotEquivalent]  if such an [x] does not exist. *)
  val left_equivalence : t -> t -> t

  (** [is_right_equivalent m n] returns [true] if and only if there is an
        invertible [x] such that [m = n * x] *)
  val is_right_equivalent : t -> t -> bool

  (** [right_equivalence m n] returns an invertible [x] such that [m = n * x] or
      raises [NotEquivalent]  if such an [x] does not exist. *)
  val right_equivalence : t -> t -> t

  (** {2 Pretty-printing} *)
  val fprint : Format.formatter -> t -> unit
end

(** The signature of a free monoid. *)
module type Free =
sig
  (** A free monoid is a monoid. *)
  include S

  (** [gen k] is the [k]-th generator of the monoid. *)
  val gen : int -> t

  (** [prepend k m] returns [return k * m]. *)
  val prepend : int -> t -> t

  (** [append m k] returns [m * return k]. *)
  val append : t -> int -> t
end

(** The free (countably-generated) monoid. *)
module FreeMonoid : Free

(** The free (countably-generated) commutative monoid. *)
module FreeCommutativeMonoid : Free

(** Free (countably-generated) trace monoids. *)
module TraceMonoid (E : sig
    (** [independant k l] is [true] if and only if the [k]-th and [l]-th
        generators commute. *)
    val independant : int -> int -> bool
  end) : Free

(** The group of integers with addition. *)
module Zplus : S with type t = Z.t

(** [MakeOpt (M)] is the monoid [M + 1], that is [M] with an additional
    absorbing element. *)
module MakeOpt (M : S) : sig
  include S with type t = M.t option
  val make : M.t -> t
end

(** [MakeProduct (M) (N)] is the product monoid of [M] and [N]. *)
module MakeProduct (M : S) (N : S) : sig
  include S with type t = M.t * N.t
  val make : M.t -> N.t -> t
  val map : (M.t -> 'a) -> (N.t -> 'b) -> t -> 'a * 'b
  val map2 : (M.t -> M.t -> 'a) -> (N.t -> N.t -> 'b) -> t -> t -> 'a * 'b
  val for_all2 : (M.t -> M.t -> bool) -> (N.t -> N.t -> bool) -> t -> t -> bool
  val exists2 : (M.t -> M.t -> bool) -> (N.t -> N.t -> bool) -> t -> t -> bool
end
