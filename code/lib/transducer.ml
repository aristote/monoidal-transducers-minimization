module Make (M : Monoid.S) =
struct
  type letter = int
  type output = M.t
  type state = int * output option
  type label = letter * output
  type transition = state * label * state

  module MOpt = Monoid.MakeOpt (M)
  module M2 = Monoid.MakeProduct (M) (M)
  let (let*) = Option.bind
  let (let+) x f = Option.map f x

  module V : Graph.Sig.COMPARABLE with type t = state = struct
    type t = state
    let compare (n1, _) (n2, _) = compare n1 n2
    let hash = fst
    let equal (n1, _) (n2, _) = n1 = n2
  end
  module V2 : Graph.Sig.COMPARABLE with type t = state * state = struct
    type t = state * state
    let compare ((n11, _), (n12, _)) ((n21, _), (n22, _)) =
      compare (n11, n12) (n21, n22)
    let hash (v1, v2) = 1 lsl (V.hash v1 - 1) * (V.hash v2 - 1)
    let equal (v11, v12) (v21, v22) = V.equal v11 v21 && V.equal v12 v22
  end

  module MakeEdge (M : Monoid.S) : Graph.Sig.ORDERED_TYPE_DFT with type t = int * M.t =
  struct
    type t = int * M.t
    let default = (0, M.one)
    let compare (a1, _) (a2, _) = compare a1 a2
  end
  module E = MakeEdge (M)
  module E2 = MakeEdge (M2)

  module G : Graph.Sig.P with type E.label = label
                          and type E.t = transition
                          and type V.t = state =
    Graph.Persistent.Digraph.ConcreteBidirectionalLabeled (V) (E)
  module G2 : Graph.Sig.P with type E.label = E2.t
                           and type V.t = V2.t =
    Graph.Persistent.Digraph.ConcreteBidirectionalLabeled (V2) (E2)

  let prod g1 g2 =
    let g1xg2_edges = G.fold_edges_e (fun e1 g1xg2 ->
        G.fold_edges_e (fun e2 g1xg2 ->
            let p1 = G.E.src e1 in
            let p2 = G.E.src e2 in
            let q1 = G.E.dst e1 in
            let q2 = G.E.dst e2 in
            let a1, m1 = G.E.label e1 in
            let a2, m2 = G.E.label e2 in
            if a1 = a2 then
              G2.add_edge_e g1xg2 G2.E.(create (p1, p2) (a1, (m1, m2)) (q1, q2))
            else
              g1xg2
          ) g2 g1xg2) g1 G2.empty in
    let g1xg2 = G.fold_vertex (fun q1 g1xg2 ->
        G.fold_vertex (fun q2 g1xg2 ->
            G2.add_vertex g1xg2 (q1, q2))
          g2 g1xg2) g1 g1xg2_edges in
    g1xg2

  type t = {
    input : (state * output) option ;
    graph : G.t ;
  }

  let empty = {
    input = None ;
    graph = G.empty ;
  }

  let singleton q0 u0 = {
    input = Some (q0, u0) ;
    graph = G.add_vertex G.empty q0 ;
  }

  let add_state q st = { st with graph = G.add_vertex st.graph q }
  let add_transition from along producing towards st =
    { st with
      graph = G.add_edge_e st.graph G.E.(create
                                           from
                                           (along, producing)
                                           towards) ;
    }

  let next st p a = G.succ_e st.graph p
                    |> List.map (fun e ->
                        let a, m = G.E.label e in
                        let q = G.E.dst e in
                        a, (q, m))
                    |> List.assoc_opt a


  let filter_states f st =
    match st.input with
    | Some (q0, _) when f q0 ->
      { st with
        graph = G.fold_vertex (fun q st_tmp ->
            if f q then st_tmp else G.remove_vertex st_tmp q)
            st.graph st.graph ;
      }
    | _ -> empty

  module MapEdges = Graph.Gmap.Edge (G) (struct
      include G
      let empty () = G.empty
    end)

  let map_edge_labels_e f graph =
    MapEdges.filter_map (fun e ->
        let a, _ = G.E.label e in
        let+ m = f e in
        G.E.(create (src e) (a, m) (dst e)))
      graph


  module IsReachable = Graph.Fixpoint.Make (G) (struct
      type vertex = G.vertex
      type edge = G.edge
      type g = G.t
      type data = bool
      let direction = Graph.Fixpoint.Forward
      let equal = (=)
      let join = (||)
      let analyze _ = Fun.id
    end)

  let reach st =
    match st.input with
    | None -> empty
    | Some (q0, _) ->
      let is_input_vertex = G.V.equal q0 in
      let is_reachable = IsReachable.analyze is_input_vertex st.graph in
      filter_states is_reachable st

  module HasOutput = Graph.Fixpoint.Make (G) (struct
      type vertex = G.vertex
      type edge = G.edge
      type g = G.t
      type data = bool
      let direction = Graph.Fixpoint.Backward
      let equal = (=)
      let join = (||)
      let analyze _ = Fun.id
    end)

  let total st1 =
    let has_direct_output q = q |> snd |> Option.is_some in
    let has_output = HasOutput.analyze has_direct_output st1.graph in
    filter_states has_output st1


  module LGCD = Graph.Fixpoint.Make (G) (struct
      type vertex = G.vertex
      type edge = G.edge
      type g = G.t
      type data = MOpt.t
      let direction = Graph.Fixpoint.Backward
      let equal = MOpt.is_right_equivalent
      let join = MOpt.( ^ )
      let analyze e d = MOpt.(Some (e |> G.E.label |> snd) * d)
    end)

  let prefix st2 =
    let output = snd in
    let lgcd = LGCD.analyze output st2.graph in
    match st2.input with
    | Some (q0, u0) when Option.is_some (lgcd q0) ->
      let factor_output q = fst q, (let* lgcd_q = lgcd q in
                                    let+ m = snd q in
                                    M.(left_divide lgcd_q m)) in
      { input = (let+ lgcd_q0 = lgcd q0 in
                 (factor_output q0, M.(u0 * lgcd_q0))) ;
        graph = st2.graph
                |> G.map_vertex factor_output
                |> map_edge_labels_e (fun e ->
                    let p = G.E.src e in
                    let q = G.E.dst e in
                    let _, m = G.E.label e in
                    let* lgcd_p = lgcd p in
                    let+ lgcd_q = lgcd q in
                    M.(left_divide lgcd_p (m * lgcd_q))) ;
      }
    | _ -> empty


  module Equivalence = Graph.Fixpoint.Make (G2) (struct
      type vertex = G2.vertex
      type edge = G2.edge
      type g = G2.t
      (* (p, q) are mapped to None when they are known to not be equivalent, to
         Some None when they are assumed equivalent but the inverstible is not
         known and Some (Some x) when they are assumed equivalent and the
         invertible is assumed to be x. *)
      type data = MOpt.t option
      let direction = Graph.Fixpoint.Backward
      let equal = Option.equal MOpt.( =~ )
      let join xoptopt1 xoptopt2 =
        let* xopt1 = xoptopt1 in
        let* xopt2 = xoptopt2 in
        match xopt1, xopt2 with
        | None, None -> Some None
        | Some x, None
        | None, Some x -> Some (Some x)
        | Some x1, Some x2 when M.(x1 =~ x2) -> Some (Some x1)
        | _ -> None
      let analyze e xoptopt =
        let* xopt = xoptopt in
        match xopt with
        | None -> Some None
        | Some x -> (* L(p') = xL(q') *)
          let _, (m, n) = G2.E.label e in
          let mx = M.(m * x) in
          (* mL(p') = ynL(q')
             <-> mxL(q') = ynL(q')
             <-> mx = yn *)
          if M.(is_left_equivalent mx n) then
            Some (Some M.(left_equivalence mx n))
          else
            None
    end)

  module StateMap = Map.Make (Int)
  let merge st3 =
    match st3.input with
    | None -> empty
    | Some (q0, u0) ->
      let equivalent_output ((_, t1), (_, t2)) = match t1, t2 with
        | None, None -> Some None
        | Some m1, Some m2 when M.(is_left_equivalent m1 m2) ->
          Some (Some M.(left_equivalence m1 m2))
        | _ -> None in
      let gxg = prod st3.graph st3.graph in
      let equivalence p q = Equivalence.analyze equivalent_output gxg (p, q) in
      let is_equivalent p q = equivalence p q |> Option.is_some in
      let represents q =
        let represents_map = 
          StateMap.singleton (V.hash q0) q0
          |> G.fold_vertex (fun p represents_map ->
              StateMap.add (V.hash p)
                (match StateMap.filter (fun _ p' ->
                     is_equivalent p p') represents_map 
                       |> StateMap.choose_opt  with
                 | Some (_, p') -> p'
                 | None -> p) represents_map)
            st3.graph
        in
        StateMap.find (V.hash q) represents_map in
      let x q = equivalence q (represents q) |> Option.get |> Option.get in
      { input = Some (q0, M.(u0 * x q0)) ;
        graph =
          G.add_vertex G.empty q0
          |> G.fold_edges_e (fun e graph ->
              let p = G.E.src e in
              let q = G.E.dst e in
              let p' = represents p in
              let q' = represents q in
              let a, m = G.E.label e in
              G.add_edge_e graph G.E.(create p' (a, M.(m * x q)) q'))
            st3.graph
      }

  let obs st = st |> total |> prefix |> merge

  let min st = st |> reach |> obs

  module Dot = Graph.Graphviz.Dot (struct
      include G
      let graph_attributes _ = []
      let default_vertex_attributes _ = []
      let vertex_name q = q |> G.V.hash |> string_of_int
      let vertex_attributes q =
        (match snd q with
         | Some m -> [ `Label (Format.asprintf "out = %a" M.fprint m) ]
         | None -> [ `Label "" ])
        @ (if fst q < 0 then [ `Width 0. ; `Height 0. ]
           else [])
      let get_subgraph _ = None
      let default_edge_attributes _ = []
      let edge_attributes e =
        let a, m = G.E.label e in
        [ `Label (if a >= 0 then
                    Format.asprintf "%d | %a" a M.fprint m
                  else
                    Format.asprintf "in = %a" M.fprint m) ]
    end)

  let fprint fmt st =
    let st = match st.input with
      | Some (q0, u0) ->
        let input = (-1, None) in
        st
        |> add_state input
        |> add_transition input (-1) u0 q0
      | None -> st in
    let graph_dot = Dot.fprint_graph fmt st.graph in
    graph_dot
end
