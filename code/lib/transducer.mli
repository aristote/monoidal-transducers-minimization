module Make (M : Monoid.S) : sig
  type letter = int
  type output = M.t
  type state = int * output option
  type label = letter * output
  type transition = state * label * state

  module G : Graph.Sig.P with type E.label = label
                          and type V.t = state

  (** {2 Creating subsequential transducers} *)
                                
  (** The type of subsequential transducers. *)
  type t = {
    input : (state * output) option ;
    graph : G.t ;
  }

  (** The empty transducer. *)
  val empty : t

  (** [singleton q0 u0] is the transducer with single state [q0] and input word
      [u0]. *)
  val singleton : state -> output -> t

  (** [add_state q st] adds the state [q] to [st]. *)
  val add_state : state -> t -> t

  (** [add_transition p a m q] adds a transition from [p] to [q] along [a]
      producing [m]. *)
  val add_transition : state -> letter -> output -> state -> t -> t

  (** {2 Computing the language of a subsequential transducer} *)

  (** [next st p a] returns the [Some (q, m)] if there is a transition from
      [p] to [q] along [a] and producing [m] in [st], and [None] otherwise. *)
  val next : t -> state -> letter -> (state * output) option

  (** {2 Higher-order functions} *)

  val filter_states : (state -> bool) -> t -> t

  (** {2 Functorial operations} *)

  (** The Reach operator. *)
  val reach : t -> t

  (** The Total operator. *)
  val total : t -> t

  (** The Prefix operator *)
  val prefix : t -> t
 
  (** The Obs operator. *)
  val merge : t -> t
  val obs : t -> t

  (** The Min operator. *)
  val min : t -> t

  (** {2 Pretty-printing} *)

  (** [fprint fmt st] draws [st] using the [dot] programming language. *)
  val fprint : Format.formatter -> t -> unit
end
