\documentclass{beamer} \usecolortheme{seahorse}
\setbeamertemplate{footline}[frame number] \setbeamertemplate{navigation
  symbols}{} \setbeamertemplate{section page} {
  \begin{centering}
    \begin{beamercolorbox}[sep=12pt,center]{part title}
      \usebeamerfont{section title}\insertsection\par
    \end{beamercolorbox}
  \end{centering}
}


%%%%%%%%%%%%%%%%%%%% PACKAGES %%%%%%%%%%%%%%%%%%%%
% appearance
\usepackage{lmodern}
\usecolortheme{orchid}
% maths
%% symbols
\usepackage{amsmath, amssymb, amsthm} \usepackage{stmaryrd} \usepackage{wasysym}
%% diagrams
\usepackage{tikz-cd} \usetikzlibrary{decorations.markings}
\tikzset{kleisli/.style={ postaction={decorate, decoration={markings, mark= at position 0.5 with {
          \draw circle[radius=1.5pt]; }}
      % \node[transform shape,xscale=.8,yscale=.4] (tempnode) {$|$}; } },
    }}}
\tikzset{
  invisible/.style={opacity=0},
  visible on/.style={alt={#1{}{invisible}}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}}%
  }
}
\usepackage{../figures/tikzit}
\input{../figures/transducers.tikzstyles}
% tables
\usepackage{diagbox}
% algorithms
\usepackage{algorithm, algorithmic}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
% links
\usepackage[nameinlink]{cleveref}
% bibliography
\usepackage[backend=biber, style=ieee, citestyle=authoryear]{biblatex}
\addbibresource{../biblatex.bib} \renewcommand*{\bibfont}{\footnotesize}
% misc
\usepackage[english]{babel} \usepackage{csquotes} \usepackage{subcaption}
\captionsetup[subfigure]{subrefformat=simple} \usepackage{appendixnumberbeamer}

%%%%%%%%%%%%%%%%%%%% COMMANDS %%%%%%%%%%%%%%%%%%%%
% operators and commands
%% monoids
\DeclareMathOperator{\lgcd}{lgcd} \DeclareMathOperator{\red}{red}
\DeclareMathOperator{\Irr}{Irr} \DeclareMathOperator{\rk}{rk}
\newcommand{\dual}[1]{{#1}^{op}} \newcommand{\inv}[1]{{#1}^{-1}}
\newcommand{\invertibles}[1]{{#1}^{\times}}
\newcommand{\LeftDivide}{\textsc{LeftDivide}}
\newcommand{\UpToInv}[1]{\textsc{UpToInv#1}} \newcommand{\LGCD}{\textsc{LGCD}}
\newcommand{\Red}{\textsc{Red}}
%% wqos
\DeclareMathOperator{\down}{{\downarrow}} \DeclareMathOperator{\up}{{\uparrow}}
\newcommand{\Ord}{\mathbf{Ord}} \newcommand{\sobr}[1]{\widehat{#1}}
%% functions
\DeclareMathOperator{\im}{im} \DeclareMathOperator{\id}{id}
%% automata
\DeclareMathOperator{\Reach}{Reach} \DeclareMathOperator{\Obs}{Obs}
\DeclareMathOperator{\Min}{Min} \DeclareMathOperator{\Total}{Total}
\DeclareMathOperator{\Prefix}{Prefix} \newcommand{\Auto}[1]{\mathbf{Auto}_{#1}}
\newcommand{\BiAuto}[1]{\mathbf{BiAuto}_{#1}} \newcommand{\In}{\mathtt{in}}
\newcommand{\Out}{\mathtt{out}} \newcommand{\St}{\mathtt{st}}
\newcommand{\A}{\mathcal{A}} \newcommand{\B}{\mathcal{B}}
\renewcommand{\H}{\mathcal{H}} \newcommand{\J}{\mathcal{J}}
\renewcommand{\L}{\mathcal{L}}
%% rings
\DeclareMathOperator{\sat}{sat} \newcommand{\Mod}[1]{#1\mathbf{Mod}}
\newcommand{\K}{\mathbb{K}} \newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}} \newcommand{\Q}{\mathbb{Q}}
%% categories
\DeclareMathOperator{\Obj}{Obj} \DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\codim}{codim} \DeclareMathOperator{\reldim}{reldim}
\DeclareMathOperator{\Id}{Id} \newcommand{\Set}{\mathbf{Set}}
\newcommand{\Kl}{\mathbf{Kl}} \renewcommand{\Vec}[1]{#1\mathbf{Vec}}
\newcommand{\C}{\mathcal{C}} \newcommand{\E}{\mathcal{E}}
\newcommand{\I}{\mathcal{I}} \newcommand{\M}{\mathcal{M}}
\renewcommand{\O}{\mathcal{O}} \newcommand{\T}{\mathcal{T}}
\newcommand{\klarrow}[1][->]{%
  \mathrel{\tikz [line width=.11ex, double distance=.33ex]
    \draw[#1, kleisli]
    (0,0) -- (0.7,0);}}
% misc
\renewcommand{\P}{\mathcal{P}} \newcommand{\N}{\mathbb{N}}
\newcommand{\card}[1]{\left| #1 \right|}
\newcommand{\Eval}[1]{\textsc{Eval}_{#1}}
\newcommand{\Equiv}[1]{\textsc{Equiv}_{#1}} \newcommand{\word}[1]{\triangleright
  #1 \triangleleft} \newcommand{\Surj}{\mathrm{Surj}}
\newcommand{\Inj}{\mathrm{Inj}} \newcommand{\Tot}{\mathrm{Tot}}
\newcommand{\Inv}{\mathrm{Inv}} \newcommand{\sem}[1]{\left\llbracket #1 \right\rrbracket}

\newcommand{\includefigure}[2][]{\includegraphics[#1]{figures/#2/figure.pdf}}
\newcommand{\makesection}{\begin{frame}\sectionpage\end{frame}}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Active learning of deterministic transducers with outputs in arbitrary
  monoids}

\author{Quentin Aristote,  IRIF, Université Paris-Cité, CNRS, INRIA}

\date{February 20th, 2024}

\maketitle

\section{Introduction: active learning of transducers}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{(deterministic) transducers}
  \begin{columns}
    \begin{column}{.35\textwidth}
      \begin{tikzpicture}[scale=2]
        \begin{pgfonlayer}{nodelayer}
          \node [style=state] (14) at (-0.75, -0.75) {1};
          \node [style=state] (15) at (0.75, -0.75) {2};
          \node [style=state] (16) at (0, 0.25) {3};
          \node [style=none] (18) at (0, -1.5) {};
          \node [style=none] (20) at (-0.75, 0.5) {};
          \node [style=label] (22) at (-0.35, -1.225) {$\varepsilon$};
          \node [style=label] (23) at (0, -0.55) {$a | \gamma \alpha \only<-12>{\beta}\only<13>{\boldsymbol{\beta}}$};
          \node [style=label, rotate=90] (26) at (-0.75, -0.15) {$b | \alpha$};
          \node [style=none] (27) at (-0.75, -1.5) {};
          \node [style=none] (28) at (0.75, -1.5) {};
          \node [style=label] (29) at (0, -0.9) {$a | \only<14>{\boldsymbol{\beta}}\gamma \beta \alpha$};
          \node [style=label] (30) at (0.55, 0.55) {$a | \gamma \alpha \beta$};
          \node [style=label] (31) at (0.35, -0.25) {$b | \only<14>{\boldsymbol{\beta}}\alpha$};
          \node [style=label] (32) at (-0.75, -1.2) {$\alpha$};
          \node [style=label] (33) at (0.75, -1.2) {$\only<14>{\boldsymbol{\beta}}\alpha$};
          \node [style=label] (34) at (-0.45, 0.5) {$\alpha$};
        \end{pgfonlayer}
        \begin{pgfonlayer}{edgelayer}
          \draw [style=arrow, in=-75, out=90, looseness=1.25] (18.center) to (14);
          \draw [style=arrow, bend left, looseness=0.50] (14) to (15);
          \draw [style=arrow, in=0, out=135] (16) to (20.center);
          \draw [style=arrow, bend left, looseness=0.50] (15) to (14);
          \draw [style=arrow, in=-90, out=90, looseness=1.50] (15) to (16);
          \draw [style=arrow] (14) to (27.center);
          \draw [style=arrow] (15) to (28.center);
          \draw [style=arrow, in=180, out=90, looseness=1.50] (14) to (16);
          \draw [style=arrow, in=15, out=-15, loop] (16) to ();
        \end{pgfonlayer}
      \end{tikzpicture}
    \end{column}
    \begin{column}{.65\textwidth}
      \begin{itemize}
      \item<2-> \makebox[3.5cm][l]{states}
        \begin{tikzpicture}[baseline=-0.5ex]
          \node[state] (A) at (0,0) {};
        \end{tikzpicture}
      \item<3-> \makebox[3.5cm][l]{input letters} $a, b, \ldots \in A$
      \item<4-> \makebox[3.5cm][l]{output letters} $\alpha, \beta, \ldots \in \Sigma$
      \item<5-> \makebox[3.5cm][l]{transitions}
        \begin{tikzpicture}[baseline=-0.5ex]
          \node[state] (A) at (0,0) {};
          \node[state] (B) at (2,0) {};
          \path (A) edge[thick, ->] node[label] {$a \mid \alpha \beta$} (B);
        \end{tikzpicture}
        % not total !
      \item<6-> \makebox[3.5cm][l]{initial state}
        \begin{tikzpicture}[baseline=-0.5ex]
          \node (A) at (0,0) {};
          \node[state] (B) at (1.5,0) {};
          \path (A) edge[thick, ->] node[label] {$\varepsilon$} (B);
        \end{tikzpicture}
      \item<7-> \makebox[3.5cm][l]{termination function}
        \begin{tikzpicture}[baseline=-0.5ex]
          \node[state] (A) at (0,0) {};
          \node (B) at (1.5,0) {};
          \path (A) edge[thick, ->] node[label] {$\alpha$} (B);
        \end{tikzpicture}
      \end{itemize}
    \end{column}
  \end{columns}
  \vspace{10pt}

  \raisebox{20.5pt}{\onslide<8->{recognizes a function}}\quad
  $\begin{matrix}
    \onslide<8->{A^* & \rightarrow & \Sigma^* \sqcup \{ \bot \} \\}
    \onslide<9->{e & \mapsto & \alpha \\}
    \onslide<10->{bb & \mapsto & \bot \\}
    \onslide<11->{aba & \mapsto & \gamma\alpha\beta~\alpha~\gamma\alpha\beta~\alpha}
  \end{matrix}$
  \vspace{10pt}

  \onslide<12->{minimal = minimal \# of states + transitions output asap}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{learning minimal transducers \parencite{vilarQueryLearningSubsequential1996}}
  given $L : A^* \rightarrow \Sigma^* \sqcup \{ \bot \}$
  \begin{itemize}
  \item input:
    \begin{itemize}
    \item \onslide<3->{$\Eval{L} : w \mapsto$ what's $L(w)$ ?}
    \item \onslide<4->{$\Equiv{L} : T \mapsto$ does $T$ recognize $L$? if not, counterexample?}
    \end{itemize}
  \item output: \onslide<2->{$\Min L$}
  \end{itemize}
  \vspace{10pt}

  \begin{columns}<5->
    \begin{column}{.3\textwidth}
      \begin{tikzpicture}[scale=1.75]
        \begin{pgfonlayer}{nodelayer}
          \node [style=none] (27) at (-0.75, -1.5) {};
          \node [style=none] (28) at (0.75, -1.5) {};
          \node [style=none] (18) at (0, -1.5) {};
          \node [style=none] (20) at (-0.75, 0.5) {};

          \onslide<7->{
            \node [style=state] (1) at (-0.75, -0.75) {$e$};
            \node [style=label] at (-0.35, -1.225) {$\only<-9>{\alpha}\only<10->{\varepsilon}$};
            \node [style=label] at (-0.75, -1.2) {$\only<-9>{\varepsilon}\only<10->{\alpha}$};
          }

          \onslide<8->{\node [style=state] (2) at (0.75, -0.75) {$\only<8-11>{?}\only<12->{a}$};}
          \onslide<8->{\node [style=label] at (0, -0.55) {$a | \only<-10>{?}\only<11->{\gamma\alpha\beta}$};}

          \onslide<13->{
            \node [style=label] at (0, -0.9) {$a | \gamma \beta \alpha$};
            \node [style=label] at (0.75, -1.2) {$\alpha$};
          }
          \onslide<13-15>{
            \node [style=label] at (-0.73, -0.2) {$b | \alpha$};
            \node [style=label] (4) at (0.3, -0.25) {$b | \alpha$};
          }

          \onslide<17->{
            \node [style=state] (3) at (0, 0.25) {$b$};
            \node [style=label] (30) at (0.55, 0.55) {$a | \gamma \alpha \beta$};
            \node [style=label, rotate=90] (26) at (-0.75, -0.15) {$b | \alpha$};
            \node [style=label] (31) at (0.35, -0.25) {$b | \alpha$};
            \node [style=label] (34) at (-0.45, 0.5) {$\alpha$};
          }
        \end{pgfonlayer}
        \begin{pgfonlayer}{edgelayer}
          \onslide<7->{
            \draw [style=arrow, in=-75, out=90, looseness=1.25] (18.center) to (1);
            \draw [style=arrow] (1) to (27.center);
          }

          \onslide<8->{
            \draw [style=arrow, bend left, looseness=0.50] (1) to (2);
          }

          \onslide<13->{
            \draw [style=arrow, bend left, looseness=0.50] (2) to (1);
            \draw [style=arrow] (2) to (28.center);
          }
          \onslide<13-15>{
            \draw [style=arrow, distance=.8em, in=117, out=63, loop] (1) to ();
            \draw [style=arrow part, in=-15, out=90] (2) to (4);
            \draw [style=arrow, in=45, out=-180] (4) to (1);
          }

          \onslide<17->{
            \draw [style=arrow, in=0, out=135] (3) to (20.center);
            \draw [style=arrow, in=-90, out=90, looseness=1.50] (2) to (3);
            \draw [style=arrow, in=180, out=90, looseness=1.50] (1) to (3);
            \draw [style=arrow, in=15, out=-15, loop] (3) to ();
          }
        \end{pgfonlayer}
      \end{tikzpicture}
    \end{column}
    \begin{column}{.7\textwidth}
      \[
        \begin{array}{c | c c c}
          \hbox{\diagbox{Pre}{Suf}} & \onslide<6->{e} & \onslide<9->{a} & \onslide<16->{b} \\
          \hline
          \onslide<6->{e} & \onslide<10->{\underline{\varepsilon}}\only<6-9>{\underline{\alpha}}\only<10->{\alpha} & \onslide<10->{\underline{\varepsilon}}\onslide<9->{\gamma \alpha \beta \alpha} & \only<9>{\notin \alpha \Sigma^*}\only<16->{\cdots} \\
          \onslide<11->{a & \underline{\gamma \alpha \beta} \alpha & \underline{\gamma \alpha \beta} \gamma \beta \alpha^2 & \onslide<16->{\gamma \alpha \beta \alpha^2} \\}
          \onslide<15->{b & \underline{\gamma \alpha \beta} \alpha & \underline{\gamma \alpha \beta} \gamma \beta \alpha^2 & \onslide<16->{\bot} \\}
          \onslide<14->{bb & \bot} & \onslide<15->{\bot} & \onslide<16->{\cdots}
        \end{array}
      \]
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{transducers with outputs in arbitrary monoids}
  \onslide<2->{if $\alpha\beta = \beta\alpha$,}

  \onslide<3->{
    \begin{tikzpicture}[scale=1.75]
      \begin{pgfonlayer}{nodelayer}
        \node [style=state] (14) at (-0.75, -0.75) {1};
        \node [style=state] (15) at (0.75, -0.75) {2};
        \node [style=none] (18) at (0, -1.5) {};
        \node [style=label] (22) at (-0.35, -1.225) {$\varepsilon$};
        \node [style=label] (23) at (0, -0.56) {$a | \gamma \alpha \beta$};
        \node [style=label] (26) at (-0.73, -0.2) {$b | \alpha$};
        \node [style=none] (27) at (-0.75, -1.5) {};
        \node [style=none] (28) at (0.75, -1.5) {};
        \node [style=label] (29) at (0, -0.9) {$a | \gamma \beta \alpha$};
        \node [style=label] (31) at (0.3, -0.25) {$b | \alpha$};
        \node [style=label] (32) at (-0.75, -1.2) {$\alpha$};
        \node [style=label] (33) at (0.75, -1.2) {$\alpha$};

        \onslide<4->{
          \node [style=label] at (1.5,-0.75) {\sim};

          \node [style=state] (1) at (2.25,-0.75) {3};
          \node [style=none] (34) at (2.25,-1.5) {};
          \node [style=none] (35) at (3,-1.5) {};

          \node [style=label] at (2.65, -1.225) {$\varepsilon$};
          \node [style=label] at (3, -0.75) {$a | \gamma \alpha \beta$};
          \node [style=label] at (2.22, -0.2) {$b | \alpha$};
          \node [style=label] at (2.25, -1.2) {$\alpha$};
        }
      \end{pgfonlayer}
      \begin{pgfonlayer}{edgelayer}
        \draw [style=arrow, in=-75, out=90, looseness=1.25] (18.center) to (14);
        \draw [style=arrow, bend left, looseness=0.50] (14) to (15);
        \draw [style=arrow, bend left, looseness=0.50] (15) to (14);
        \draw [style=arrow] (14) to (27.center);
        \draw [style=arrow] (15) to (28.center);
        \draw [style=arrow, distance=.8em, in=117, out=63, loop] (14) to ();
        \draw [style=arrow part, in=-15, out=90] (15) to (31);
        \draw [style=arrow, in=45, out=-180] (31) to (14);

        \onslide<4->{
          \draw [style=arrow, in=-75, out=90, looseness=1.25] (35.center) to (1);
          \draw [style=arrow] (1) to (34.center);
          \draw [style=arrow, distance=.8em, in=117, out=63, loop] (1) to ();
          \draw [style=arrow, distance=.8em, in=-30, out=30, loop] (1) to ();
        }
      \end{pgfonlayer}
    \end{tikzpicture}
  }
  \vspace{10pt}

  \onslide<5->{learning $\Min L$ with $L : A^* \rightarrow \langle \Sigma \mid
    \alpha\beta = \beta\alpha \rangle \sqcup \{ \bot \}$:}

  \begin{theorem}<6->
    as for $\Sigma^*$, except
    \begin{itemize}
    \item<7-> check for equality up to \textbf{invertible} elements
    \item<8-> compute \textbf{greatest common left divisor} instead of longest
      common prefix
    \end{itemize}
    \onslide<9->{correct for any output monoid that has \textbf{left gcds} and
      is \textbf{cancellative}}\onslide<10->{; terminates for
      \textbf{right-noetherian} monoids}
  \end{theorem}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Functorial approach to minimality}
\makesection

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{automata as functors
    \parencite{petrisanAutomataMinimizationFunctorial2020}}
  \begin{columns}
    \column{\dimexpr\paperwidth-1em}
    \begin{center}
      % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZARgBoAGAXVJADcBDAGwFcYkQBlEAX1PU1z5CKchWp0mrdgB1pwAFSzuPPiAzY8BIgCYxNBizaIQs4LPoBjC6VlgYAcwAE5q0p7iYUe-CKgAZgBOEAC2SGQgOBBIohKG7PLO0sH0aHCRjnAA+sBYYLjKvP5BoYgxkUi6sVLGLhYA-CpFIdE05aU0jBAQaETEAByifkxwMOKM9ABGMIwACgKawiABWPYAFjgg+pJGJtKwjDj0mfTu3EA
      \begin{tikzcd}[column sep=huge]
        |[visible on=<4->]| \{*\} \arrow[visible on=<4->, r, "* \mapsto
        s_{init}"] & |[visible on=<2->]| S \arrow[visible on=<5->, r, "acc?"]
        \arrow[visible on=<3->, "\delta_a"', loop, distance=2em, in=125, out=55]
        & |[visible on=<5->]| {\{acc,\neg acc\}}
      \end{tikzcd}
    \end{center}
  \end{columns}
  \begin{itemize}
  \item<6-> $(
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB12BJQgX1PSZc+QigCM5KrUYs2nAMo4Q-QdjwEiAJknV6zVog7sA8kyW8pMKAHN4RUADMAThAC2SMiBwQkE6frl2HCcsOjBrBhgQ6wALcwEQZzdfam8UkAYICDQiMQAOMgdGOBgpBjoAIxgGAAUhdVEQaLiQXRkDEDplBKT3RD80xG1-WUNOYNDwyMiHcwpeIA
    \begin{tikzcd}
      \In \arrow[r, "\triangleright"] & \St \arrow["a"', loop, distance=2em,
      in=125, out=55] \arrow[r, "\triangleleft"] & \Out
    \end{tikzcd}
    )$ in $\Set$
  \item<7-> \emph{non-deterministic automata} = diagram in $\mathbf{Rel}$
  \item<7->\emph{weighted automata} = diagram in $\Vec{\K}$
  \end{itemize}
  \end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{automata as functors: minimality (\textit{ibid.})}
  \begin{itemize}
  \item<2-> $(\E, \M) = (\Surj, \Inj)$ is a \emph{factorization system}
    in $\Set$: \\ \uncover<3->{each $X \xrightarrow{f} Y$ has a
      unique factorization} \begin{center}
      % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZARgBoAGAXVJADcBDAGwFcYkQAdDrAWwAoAZgEoQAX1LpMufIRQAmCtTpNW7LgEEA5Hy4BlHCPGTseAkXKKaDFm0ScO6nR32GlMKAHN4RUAIBOEDxIFiA4EEhkIAAWMPRQSGDMjIxWKrYgPAAEXFhg2RwAsiA0OPRYjOyl5WISIP6BSAqh4Ygh1qp2MPm5+QCixdGx8XY4AO4QMXEIopSiQA
      \begin{tikzcd}[visible on=<3->]
        X \arrow[r, "e \in \E", two heads] & \im(f) \arrow[r, "m \in \M",
        tail] & Y
      \end{tikzcd}
    \end{center}
  \item \uncover<4->{for automata:}
    \begin{center}
      % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBoBGAXVJADcBDAGwFcYkQBBAPQCoQBfUuky58hFOQrU6TVuwA68gEox6AYwAWijgKEgM2PASIAmUsWkMWbRCG27hhsUQDMUmlbm3FAeQBGcPaCjqLGKAAs7jLW7CZcwNw8-A76IkbiyGYmlrI2dvIAslhgigAyAtIwUADm8ESgAGYAThAAtkhkIDgQSJIgGqpQ7DgA7hAD9FAIwSDNbb003UhmXfRYjMNrGzNz7YgArIs9iG6r65vnOy17p0uIkWcbtjhbKbvLR0inE0PPYz-TPTvRCdO4rTx5EYAAkUrXoaDg3Rh8g4AApFDgmlh6GBqowYFjqhocFCRgBKEA0PwwMC-FzEK7zRArO4PCHsODIuEIpFlAD6CCpNLpDKB1wWXWOh36g2G-0GgMa4pBn3uQtpSAAtAA2Trs7zyOE4DRNVrAAoATw060YmoAcgToDB+IhobD4YiIMjirRgCN+GVKSBGPRqYwAAppZy2QnEir8IA
      \begin{tikzcd}
        & & |[visible on=<4->]|
        S \arrow[visible on=<6->, rd, two heads] \arrow[visible on=<6->, rrd, "s \mapsto \L_s", bend left] &                        &         \\
        |[visible on=<5->]| A^* \arrow[visible on=<5->, r, two heads]
        \arrow[visible on=<5->, rru, "w \mapsto \delta_w(s_{init})", bend
        left] \arrow[visible on=<7->, rrrr, "\mathrm{Myhill-Nerode}:w \mapsto
        \inv{w}\L"', bend right=60] & |[visible on=<5->]| \Reach S
        \arrow[visible on=<5->, ru, tail] \arrow[visible on=<7->, rd, two heads]
        & & |[visible on=<6->]| \Obs S \arrow[visible on=<6->, r, tail] &
        |[visible
        on=<6->]| 2^{A^*} \\
        & & |[visible on=<7->]| \Min\L \arrow[visible on=<7->, ru, tail] & &
      \end{tikzcd}
    \end{center}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{automata as functors: algorithms}
  \begin{itemize}
  \item<2-> minimization (new)
    \begin{itemize}
    \item<3-> computing $\Reach$
    \item<4-> computing $\Obs$
    \end{itemize}
  \item<5-> active learning \parencite{colcombetLearningAutomataTransducers2020}
  \item<6-> complexity results (new)
  \end{itemize}
  \begin{theorem}<7->[the true one this time]
    \begin{itemize}
    \item<8-> transducers with outputs in arbitrary monoids can be modeled as functors
    \item<9-> the framework applies when the output monoid has
      \textbf{left-gcds}, is \textbf{right-cancellative}; termination when
      \textbf{right-noetherian}
    \end{itemize}
  \end{theorem}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusion}
\makesection

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{conclusion}
  \begin{itemize}
  \item functorial approach to automata applies to transducers with outputs in
    arbitrary monoids
  \item under reasonable conditions, minimality
  \item algorithms, in particular generalization of
    \parencite{vilarQueryLearningSubsequential1996}'s active learning
  \item factorization systems explain intermediate computations
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\appendix

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Monoidal transducers as functors}
\makesection

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{transducers with output in \only<-5>{$\Sigma^*$}\onslide<6->{a
      monoid $M$}}
  \begin{columns}
    \begin{column}{.3\textwidth}
      \includegraphics[scale=1.3]{../figures/transducer2_hypothesis/figure.pdf}
    \end{column}
    \begin{column}{.7\textwidth}
      \begin{itemize}
      \item<2->
        $(% https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB12BJQgX1PSZc+QigCM5KrUYs2nAMo4Q-QdjwEiAJknV6zVog7sA8kyW8pMKAHN4RUADMAThAC2SMiBwQkE6frl2HCcsOjBrBhgQ6wALcwEQZzdfam8UkAYICDQiMQAOMgdGOBgpBjoAIxgGAAUhdVEQaLiQXRkDEDplBKT3RD80xG1-WUNOYNDwyMiHcwpeIA
        \begin{tikzcd}
          \In \arrow[r, "\triangleright"] & \St \arrow["a"', loop, distance=2em,
          in=125, out=55] \arrow[r, "\triangleleft"] & \Out
        \end{tikzcd})$ in $\Kl(\T)$
      \item<3-> $\Kl(\T)$ = sets with functions $X \klarrow Y \equiv X
        \rightarrow \only<-5>{\Sigma^*}\only<6->{M} \times Y + 1$ % TODO: fix width
      \item<4-> $\A
        = % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB13gACAKm84BfEINLpMufIRQBGclVqMWbTgEEAFJwDKOAJQixIDNjwEiAJnnV6zVog5c+A9sMEKYUAObwioAGYAThAAtkhkIDgQSHKKtirsGpw4AVh0YJ4MMCmeABZ6Bv5BoYgxkUiWscr2aprsyanpmZl++aKFIdHUZSXUDBAQaEQyABxkfoxwMAoMdABGMAwAChKm0iDZeSDWSnYOGnT6boJAA
        \begin{tikzcd}
          \{ * \} \arrow[r, kleisli, "\A(\triangleright)"] & \A(\St) \arrow[r,
          kleisli, "\A(\triangleleft)"] \arrow[kleisli, "\A(a)"', loop,
          distance=2em, in=125, out=55] & \{ * \}
        \end{tikzcd}$
      \item<5-> $\L
        = % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB13gACAKm84BfEINLpMufIRQBGclVqMWbTj35CRCmFADm8IqABmAJwgBbJGRA4ISOYuatEHdgEEAFJxzGsdMDoYYHx0ACxxuAHcBdm9ff0DAwxwASk1BIA
        \begin{tikzcd}
          \{ * \} \arrow[r, kleisli, "\A(\triangleright w \triangleleft)"] & \{
          * \}
        \end{tikzcd}$
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{condition for the final transducer to exist}
  \begin{itemize}
  \item<2-> $1 = \{ * \}$ has an $A^*$-power in $\Kl(\T)$
    \[ % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAEYQBfU9TXfIRTtyVWoxZsAOlICSAJ3kAKAIIA9AFSkABAFkAlN14gM2PASIAmUdXrNWiDtzEwoAc3hFQAM3kQAtkhkIDgQSCIgDFhgDiBQdHAAFq4gthKxMgx0YG4MMNoAMgD6AO7aMvLZuTBFwGUy0drqGlxGPn6BiBGhSNbi9tJSaFilzlxAA
      \begin{tikzcd}
        {\Irr(A^*, M)} \arrow[r, kleisli, "\pi_w"] & 1
      \end{tikzcd}
    \]
  \item<3-> $\Leftrightarrow$ there are $\lgcd : (A^* \rightarrow M + 1) \rightarrow
    M + 1$ and $\red : (A^* \rightarrow M + 1) \rightarrow (A^* \rightarrow M +
    1)$ s.t.
    \begin{itemize}
    \item $L = \lgcd(L)\red(L)$
    \item $\upsilon \red K = \nu \red L$ implies $\upsilon = \nu$ and $\red K =
      \red L$
    \end{itemize}
  \item<4-> true in particular when $M$ is cancellative and every family has a
    unique greatest common left divisor
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{initial and final transducers recognizing a language $\L$}
  \begin{center}
    \[ % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBoAmAXVJADcBDAGwFcYkQAdD4AAgCoeXAL4ghpdJlz5CKACwVqdJq3ZdeA4aPEgM2PASLlSxRQxZtEIAIIA9Plol7ph0rNPKLnDgEkATr4AKWz5SHgBZAEpRRRgoAHN4IlAAM18IAFskIxAcCCyaRggINCIARgAOMmSmOBhFRnoAIxhGAAVJfRkQXyw4gAscEBozFUsAd0EOdPo0OFyeAK4YWaxCsFCx+iixFLTMxABmGlykI5BC4qIqmrqCppb2pwNLHv7B4Y92ABlJ6dn5xYcRhxADGUEBWDAtGA9CEXwioS4vliEKhMLhEW22lSGSQZByeUQ2RGng0UxmcwgCyWKzWoRgWN2uKJx0JpQ+5nYEy4f0p1I4X0BYwgvigwDGQgR-EZIBx+3xJ0OHNGIDJvIBXGBYMmX0RHGRUB1UTuzTaHWcL16Awcsr2+QJp2Vnm55P+VMBWvBXEh0IlXHheoNqN9Qn9mKG53uZqeXUh2FgNrljodiHZSk5lh+PIpAMFDNCfGNkdNjykz26VsGQkoQiAA
    \begin{tikzcd}
      && |[visible on=<2->]| \{ * \} & \\ \\ \\
      |[visible on=<3->]| A^*
      \arrow[kleisli, visible on=<3->, rruuu, "{w \mapsto \L(\triangleright w
        \triangleleft)}", bend left]
      \arrow[kleisli, visible on=<3->, loop, distance=3em, in=150, out=-150, "{w \mapsto
        (\varepsilon, wa)}"]
      \arrow[kleisli, rrr, visible on=<4->, dashed, "{w \mapsto
        \begin{array}{c}
          (\lgcd(\inv{w}\L), \\
          \red(\inv{w},\L))
        \end{array}
      }"]
      && & |[visible on=<2->]|
      \Irr(A^*,M)
      \arrow[kleisli, visible on=<2->, luuu, "{L \mapsto (L(e),*)}"', bend
      right]
      \arrow[kleisli, visible on=<2->, loop, distance=3em, in=-30, out=30, "{L \mapsto
        % TODO: space
        \begin{array}{c}
          (\lgcd(\inv{a}L, \\
          \red(\inv{a}L))
        \end{array}}
      "]
      \\ \\ \\
      && |[visible on=<2->]| {\{ * \}}
      \arrow[kleisli, visible on=<2->, ruuu, "{* \mapsto (\lgcd\L, \red\L)}"', bend right]
      \arrow[kleisli, visible on=<3->, lluuu, "{* \mapsto (\varepsilon, e)}", bend left]
      &&
      % &  & A^* \arrow["{w \mapsto (\varepsilon, wa)}"', loop, distance=2em, in=125, out=55] \arrow[rrdd, "{w \mapsto (\L(\word{w}), *)}"] \arrow[dddd, "{w \mapsto (\lgcd(\inv{w}\L), \red(\inv{w}\L))}" description] &  &         \\
      % &  &                                                                                                                                                                                                         &  &         \\
      % \{ * \} \arrow[rruu, "{* \mapsto (\varepsilon, e)}"] \arrow[rrdd, "{* \mapsto (\lgcd \L, \red \L)}"'] &  &                                                                                                                                                                                                         &  & \{ * \} \\
      % &  &                                                                                                                                                                                                         &  &         \\
      % & & {\Irr(A^*, M)} \arrow["{L \mapsto (\lgcd(\inv{a}L),
      %   \red(\inv{a}L))}"', loop, distance=2em, in=305, out=235] \arrow[rruu,
      % "{L \mapsto (L(e), *)}"'] & &
    \end{tikzcd}
    \]
  \end{center}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{factorization systems in $\Kl(\T)$}
  \begin{center}
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB12BBEAX1PSZc+QigCM5KrUYs2nACoQcjAASce-QdjwEiAJknV6zVog7sACgCcYAMywAPNdz4CQGbSKIBmQ9JNy7ADyAEZwzhpuHsK6KAAsfsayZpwAklZWABRcAHoAVKQqALIAlK5aMaIkpGJSSaYg5e5COlW+tUYyDU3RrUQJHf7JjZrNnrHVenVdbD0tXigGU50BZnPjVQnLQ92jvQvV3tOrI1HzExJHK8Prlf2kVzuzvFIwUADm8ESgtlYQALZIMggHAQJASEAACxgdCgbBwAHcINDYQhRr8AeDqKCkAYoTC4WZEciCQhsXQsAx4RSqei-oDEHicYhfCCadTKSBqCjCSCkTy0W4MQzWcyEmzOUT2XTMYgAKzYsGIABs3IJbEgYFY1AYdBCMAYFnOohAWDA2FgXKeKXYAGUmFYAFZNYVIVUgpUAdjVsI1BG1IF1+sNxrYZotAfqgVSYEdzgAxnQ0M4YzQE0nnIocC76UgAByKpAATh9vM1AaDBqNGzD5qwluuDU49qd6eTaVjOdlJY94OBPL9WqtlZDNbM4frkZmNtTbczSi7DLEEOZYjxI+rd3HdYb+N9ZnLVqjNpbcc4ifb7BjZ-YF5TYBoi-BTKVYlZx-MWeHeqroe3EatAcD39PgKF4IA
    \onslide<2->{\[\begin{tikzcd}[row sep=tiny, ampersand replacement=\&]
        \A
        \arrow[kleisli, visible on=<-2>, rrrr]
        \arrow[kleisli, "\Tot^c", visible on=<3->, r, two heads]
        \& |[visible on=<3->]| \Total \A
        \arrow[kleisli, visible on=<3-4>, rrr]
        \arrow[kleisli, "\Inv^c", visible on=<5->, r, two heads]
        \& |[visible on=<5->]| \Prefix \A
        \arrow[kleisli, visible on=<5-7>, rr]
        \arrow[kleisli, "\Inj^c", visible on=<8->, r, two heads]
        \& |[visible on=<8->]| {\Obs \A}
        \arrow[kleisli, visible on=<8-9>, r]
        \arrow[kleisli, "\Surj^c", visible on=<10->, r, tail]
        \& {\Irr(A^*, M)} \\
        {} \arrow[kleisli, visible on=<11->, rrr, "\Surj" description, no head] \& \& \&
        {} \arrow[kleisli, visible on=<11->, r, "\Inj \cap \Inv \cap \Tot" description, no head] \& {} \\
        % {} \arrow[visible on=<4->, rr, "\Surj \cap \textcolor{red}{\Inj}"
        % description, no head] \& \& {} \arrow[visible on=<4->, rr,
        % "\textcolor{blue}{\Inv} \cap \Tot" description, no
        % head] \& \& {} \\
        % {} \arrow[visible on=<6->, r, "\Surj \cap \textcolor{red}{\Inj} \cap
        % \textcolor{blue}{\Inv}" description, no head] \& {} \arrow[visible
        % on=<6>, rrr, "\Tot" description, no head] \& \& \& {}
      \end{tikzcd}\]}
  \end{center}
  \onslide<2->{if $\alpha\beta = \beta\alpha$ and $\alpha\gamma = \gamma\alpha$,
  \begin{figure}
    \centering
    \begin{subfigure}{.5\linewidth}
      \centering
      \begin{tikzpicture}[scale=2]
        \begin{pgfonlayer}{nodelayer}
          \node [style=state] (14) at (-0.75, -0.75) {1};
          \node [style=state] (15) at (0.75, -0.75) {2};
          \node [style=none] (18) at (0, -1.5) {};
          \node [style=label] (22) at (-0.35, -1.225) {$\only<-6>{\varepsilon}\only<7>{\boldsymbol{\alpha}}\only<8->{\alpha}$};
          \node [style=label] (23) at (0, -0.56) {$a | \gamma \only<-5,8->{\alpha}\only<6-7>{\boldsymbol{\alpha}} \beta$};
          \node [style=label] (26) at (-0.73, -0.2) {$b | \only<-5,8->{\alpha}\only<6-7>{\boldsymbol{\alpha}}$};
          \node [style=none] (27) at (-0.75, -1.5) {};
          \node [style=none] (28) at (0.75, -1.5) {};
          \node [style=label] (29) at (0, -0.9) {$a | \gamma \beta \only<-5,8->{\alpha}\only<6-7>{\boldsymbol{\alpha}}$};
          \node [style=label] (31) at (0.3, -0.25) {$b | \only<-5,8->{\alpha}\only<6-7>{\boldsymbol{\alpha}}$};
          \node [style=label] (32) at (-0.75, -1.2) {$\only<-5>{\alpha}\only<6>{\boldsymbol{\alpha}}\only<7->{\varepsilon}$};
          \node [style=label] (33) at (0.75, -1.2) {$\only<-5>{\alpha}\only<6>{\boldsymbol{\alpha}}\only<7->{\varepsilon}$};
        \end{pgfonlayer}
        \begin{pgfonlayer}{edgelayer}
          \draw [style=arrow, in=-75, out=90, looseness=1.25] (18.center) to (14);
          \draw [style=arrow, bend left, looseness=0.50] (14) to (15);
          \draw [style=arrow, bend left, looseness=0.50] (15) to (14);
          \draw [style=arrow] (14) to (27.center);
          \draw [style=arrow] (15) to (28.center);
          \draw [style=arrow, distance=.8em, in=117, out=63, loop] (14) to ();
          \draw [style=arrow part, in=-15, out=90] (15) to (31);
          \draw [style=arrow, in=45, out=-180] (31) to (14);
        \end{pgfonlayer}
      \end{tikzpicture}
      \caption*{$\only<4-6>{\Total}\only<7->{\Prefix}\A$}
    \end{subfigure}
    \onslide<9->{\begin{subfigure}{.4\linewidth}
      \centering
      \begin{tikzpicture}[scale=2]
        \begin{pgfonlayer}{nodelayer}
          \node [style=state] (1) at (2.25,-0.75) {3};
          \node [style=none] (34) at (2.25,-1.5) {};
          \node [style=none] (35) at (3,-1.5) {};

          \node [style=label] at (2.65, -1.225) {$\varepsilon$};
          \node [style=label] at (3, -0.75) {$a | \gamma \beta \alpha$};
          \node [style=label] at (2.22, -0.2) {$b | \varepsilon$};
          \node [style=label] at (2.25, -1.2) {$\alpha$};
        \end{pgfonlayer}
        \begin{pgfonlayer}{edgelayer}
          \draw [style=arrow, in=-75, out=90, looseness=1.25] (35.center) to (1);
          \draw [style=arrow] (1) to (34.center);
          \draw [style=arrow, distance=.8em, in=117, out=63, loop] (1) to ();
          \draw [style=arrow, distance=.8em, in=-30, out=30, loop] (1) to ();
        \end{pgfonlayer}
      \end{tikzpicture}
      \caption*{$\Obs \A$}
    \end{subfigure}}
  \end{figure}}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
